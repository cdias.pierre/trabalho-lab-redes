import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { first } from 'rxjs';

import { Course } from './../model/course';

@Injectable({
  providedIn: 'root',
})
export class CoursesService {
  private readonly API = '/api/courses/';

  constructor(private httpClient: HttpClient) {}

  //Método de consulta
  findCourses() {
    return this.httpClient.get<Course[]>(this.API).pipe(first());
  }

  //Método para salvar curso
  save(course: Course) {
    return this.httpClient.post<Course>(this.API, course);
  }
}
